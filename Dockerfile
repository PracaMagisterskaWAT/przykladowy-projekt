FROM openjdk:17.0.2-jdk-buster

RUN groupadd -r app && useradd -r -s /bin/false -g app app && \
    apt-get update && \
    apt-get install --no-install-recommends -y curl && \
    rm -rf /var/cache/apt/archives && \
    rm -rf /var/lib/apt/lists/*

# copy the pre-generated jar files
WORKDIR /app

COPY target/demo-0.0.1-SNAPSHOT.jar /app/demo-0.0.1-SNAPSHOT.jar
RUN chown -R app:app /app

EXPOSE 8242

HEALTHCHECK CMD curl -G -k -s -f http://localhost:8242/health || exit 1

USER app
# and finally, set the service to be started
CMD ["sh", "-c", "java -jar /app/demo-0.0.1-SNAPSHOT.jar -Xmx$SERVICE_MEMORY"]