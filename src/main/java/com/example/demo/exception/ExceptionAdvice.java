package com.example.demo.exception;

import com.example.demo.exception.dto.BaseError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
@ControllerAdvice
public class ExceptionAdvice {

    @ResponseBody
    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<BaseError> handleResponseStatusException(ResponseStatusException ex) {
        log.error("{}", ex.getMessage());
        return new ResponseEntity<>(new BaseError(ex.getReason()), ex.getStatus());
    }

}
