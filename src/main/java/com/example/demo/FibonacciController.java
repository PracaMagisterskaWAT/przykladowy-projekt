package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Slf4j
@RestController
@RequestMapping("/api/fib")
public class FibonacciController {

    @GetMapping("/{n}")
    public Integer getFibonacciNumber(@PathVariable Integer n) {
        log.info("REST Get Fibonacci number for n = {}.", n);
        var result = 1;
        if (n < 1) {
            throw new ResponseStatusException(BAD_REQUEST, "Number less than 1");
        }
        if (n > 46) {
            throw new ResponseStatusException(BAD_REQUEST, "Number more than 46");
        }
        if (n > 1) {
            var fibonacciNumber = 1;
            var prevNumber = 1;

            for (int i = 2; i < n; i++) {
                var temp = fibonacciNumber;
                fibonacciNumber += prevNumber;
                prevNumber = temp;
            }
            result = fibonacciNumber;
        }
        log.info("The result for n = {} is {}", n, result);
        return result;
    }

}
