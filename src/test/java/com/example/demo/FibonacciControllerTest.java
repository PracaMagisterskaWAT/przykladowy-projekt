package com.example.demo;

import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class FibonacciControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void getFibonacciNumberTest() {

        val output = List.of(1, 1, 2, 3, 5, 8, 13, 21, 34,
                55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181);
        boolean result = true;
        for (int i = 1; i < output.size() - 1; i++) {
            val url = "/api/fib/" + i;
            val body = restTemplate.getForObject(url, Integer.class);

            result = body.equals(output.get(i - 1));

        }
        assertTrue(result);
    }

    @Test
    void getFibonacciNumberForLessThanOneTest() {
        val url = "/api/fib/-1";
        val responseEntity = restTemplate.getForEntity(url, Object.class);
        assertThat(responseEntity.getStatusCode()).isEqualTo(BAD_REQUEST);
    }

    @Test
    void getFibonacciNumberForMoreThan46Test() {
        val url = "/api/fib/47";
        val responseEntity = restTemplate.getForEntity(url, Object.class);
        assertThat(responseEntity.getStatusCode()).isEqualTo(BAD_REQUEST);
    }

}
